﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using Albakom.Besa.Authentication.Infrastructure;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace Albakom.Besa.Authentication
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api1", "My API"),
                new ApiResource("tki-adapter-test", "test to access tki adapter"),
                new ApiResource("tki-adapter", "access to tki adapter"),

            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(AuthClientOptions options)
        {
            // client credentials client
            return new List<Client>
            {
                new Client
                {
                    ClientId = "besa-spa",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api1" },
                    AllowAccessTokensViaBrowser = true,
                    RedirectUris =
                    {
                        $"{options.WebAppRedirectUrl}/loginSuccess",
                    },
                    PostLogoutRedirectUris =
                    {
                        $"{options.WebAppLogoutRedirectUrl}/logoutSuccess",
                    }
                },
                new Client
                {
                    ClientId = "besa-server-tester",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("sicheresPassw0rd".Sha256()),
                    },
                    AllowedScopes = { "tki-adapter-test" },
                },
                 new Client
                {
                    ClientId = "besa-service",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("kLSk7YXsndpcVtt8DyBm".Sha256()),
                    },
                    AllowedScopes = { "tki-adapter" },
                }
            };
        }
    }
}
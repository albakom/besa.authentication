﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Albakom.Besa.Authentication.Data;
using Albakom.Besa.Authentication.Models;
using Albakom.Besa.Authentication.Services;
using System.Reflection;
using IdentityServer4.EntityFramework.DbContexts;
using Albakom.Besa.Authentication.Infrastructure;
using Microsoft.Extensions.Logging;

namespace Albakom.Besa.Authentication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            MailjetOptions mailjetOption = Configuration.GetSection("MailjetOptions").Get<MailjetOptions>();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("UserStorageConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                // example of setting options
                options.Tokens.ChangePhoneNumberTokenProvider = "Phone";

                // password settings chosen due to NIST SP 800-63
                options.Password.RequiredLength = 8; // personally i'd prefer to see 10+
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.AddTransient<MailjetTemplateResolver>((provider) =>  new MailjetTemplateResolver(mailjetOption.Templates)  );

            // Add application services.
            services.AddTransient<IEmailSender, MailjetBasedEmailSender>( (provider) => new MailjetBasedEmailSender(
                provider.GetService<ILogger< MailjetBasedEmailSender>>(),
                provider.GetService<MailjetTemplateResolver>(),
                mailjetOption.ApiKey, mailjetOption.ApiSecret));

            services.AddMvc();

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            AuthClientOptions authClientOptions = Configuration.GetSection("AuthClientOptions").Get<AuthClientOptions>();
            // configure identity server with in-memory stores, keys, clients and scopes
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()

                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddInMemoryClients(Config.GetClients(authClientOptions))
                //.AddConfigurationStore(options =>
                // {
                //     options.ConfigureDbContext = builder =>
                //         builder.UseSqlite(Configuration.GetConnectionString("TokenCofigurationConnection"),
                //             sql => sql.MigrationsAssembly(migrationsAssembly));
                // })
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlite(Configuration.GetConnectionString("OperationCofigurationConnection"),
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30;
                })
                //.AddInMemoryPersistedGrants()
                .AddAspNetIdentity<ApplicationUser>();

            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    options.ClientId = "998042782978-s07498t8i8jas7npj4crve1skpromf37.apps.googleusercontent.com";
                    options.ClientSecret = "HsnwJri_53zn7VcO1Fm7THBb";
                });

            services.AddCors((options) =>
           {

           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseCors((options) =>
           {
               options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials();
           });

            // app.UseAuthentication(); // not needed, since UseIdentityServer adds the authentication middleware
            app.UseIdentityServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            InitializeDatabase(app);
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            }
        }
    }
}

﻿using Albakom.Besa.Authentication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Infrastructure
{
    public class MailjetOptions
    {
        #region Properties

        public String ApiKey { get; set; }
        public String ApiSecret { get; set; }

        public Dictionary<EMailReasons, Int32> Templates { get; set; }
        
        #endregion

        #region Constructor

        public MailjetOptions()
        {

        }

        #endregion
    }
}

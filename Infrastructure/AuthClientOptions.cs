﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Infrastructure
{
    public class AuthClientOptions
    {
        #region Properties

        public String WebAppRedirectUrl { get; set; }
        public String WebAppLogoutRedirectUrl { get; set; }

        #endregion

        #region Constructor

        public AuthClientOptions()
        {

        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services
{
    public enum EMailReasons
    {
        PasswordForgotten = 1,
        ConfirmNewEmailAdress = 2

    }
}

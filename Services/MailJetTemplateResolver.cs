﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services
{
    public class MailjetTemplateResolver
    {
        #region Fields

        private IDictionary<EMailReasons, Int32> _templateMapper;

        #endregion

        #region Constructor

        public MailjetTemplateResolver(IDictionary<EMailReasons, Int32> templateMapper)
        {
            _templateMapper = templateMapper ?? throw new ArgumentNullException(nameof(templateMapper));
        }

        #endregion

        #region Methods

        public Boolean CheckIfTemplateIsAvaible(EMailReasons reason)
        {
            return _templateMapper.ContainsKey(reason);
        }

        public Int32 GetTemplateId(EMailReasons reason)
        {
            if(CheckIfTemplateIsAvaible(reason) == false)
            {
                throw new ArgumentException($"no template for reason {reason} found");
            }
            else
            {
                return _templateMapper[reason];
            }
        }

        #endregion
    }
}

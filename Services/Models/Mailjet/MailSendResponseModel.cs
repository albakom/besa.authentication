﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailSendResponseModel 
    {
        #region Properties

        public Int32 StatusCode { get; set; }
        public Boolean HasError { get; set; }
        public String Error { get; set; }

        internal string GetErrorInfo()
        {
            return Error;
        }

        #endregion

        #region Constructor

        #endregion
    }
}

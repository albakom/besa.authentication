﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailJetRecipientModel
    {
        #region Properties

        public String Email { get; set; }
        public String Name { get; set; }

        #endregion

        #region Constructor

        public MailJetRecipientModel() : this(String.Empty, String.Empty)
        {

        }

        public MailJetRecipientModel(String mail) : this(mail,String.Empty)
        {

        }

        public MailJetRecipientModel(String email, String name)
        {
            Name = name;
            Email = email;
        }

        #endregion
    }
}

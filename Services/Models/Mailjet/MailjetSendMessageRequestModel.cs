﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailjetSendMessageRequestModel
    {
        #region Properties

        public IEnumerable<MailJetSendMessageItemModel> Messages { get; set; }

        #endregion

        #region Constructor

        public MailjetSendMessageRequestModel()
        {
            Messages = new List<MailJetSendMessageItemModel>();
        }

        public MailjetSendMessageRequestModel(MailJetSendMessageItemModel item) : this()
        {
            Messages = new List<MailJetSendMessageItemModel>() { item };
        }

        #endregion
    }
}

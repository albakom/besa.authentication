﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailjetResponseRecipientModel
    {
        #region Properties

        public String Email { get; set; }
        public Guid MessageUUID { get; set; }
        public UInt64 MessageID { get; set; }
        public String MessageHref { get; set; }

        #endregion

        #region Constructor

        public MailjetResponseRecipientModel()
        {

        }
       
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailJetSendMessageItemModel
    {
        #region Properties

        public MailJetRecipientModel From { get; set; }
        public IEnumerable<MailJetRecipientModel> To { get; set; }
        public Int32 TemplateID { get; set; }
        public Boolean TemplateLanguage { get; set; }
        public String Subject { get; set; }
        public IDictionary<String,Object> Variables { get; set; }

        #endregion

        #region Constructor

        public MailJetSendMessageItemModel()
        {
            From = new MailJetRecipientModel();
            To = new List<MailJetRecipientModel>();
            Variables = new Dictionary<String, Object>();
        }

        public MailJetSendMessageItemModel(MailJetRecipientModel from, MailJetRecipientModel to) : this()
        {
            From = from;
            To = new List<MailJetRecipientModel>() { to };
        }

        public MailJetSendMessageItemModel(MailJetRecipientModel from, MailJetRecipientModel to, Int32 templateId, IDictionary<String, Object> variables) : this(from,to)
        {
            TemplateID = templateId;
            TemplateLanguage = true;
            Variables = variables;
        }

        #endregion
    }
}

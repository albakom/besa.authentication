﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public class MailJetSendResponseModel : MailSendResponseModel
    {
        #region Properties

        public IEnumerable<MailjetResponseMessage> Messages { get; set; }


        #endregion

        #region Constructor

        public MailJetSendResponseModel()
        {
            Messages = new List<MailjetResponseMessage>();
        }
        
        #endregion
    }
}

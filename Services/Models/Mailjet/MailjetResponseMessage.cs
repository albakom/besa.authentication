﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models.Mailjet
{
    public enum MailjetResponseMessageStatus
    {
        Success,
    }

    public class MailjetResponseMessage
    {
        #region Properties

        public MailjetResponseMessageStatus Status { get; set; }
        public IEnumerable<MailjetResponseRecipientModel> To { get; set; }

        #endregion

        #region Constructor

        public MailjetResponseMessage()
        {
            To = new List<MailjetResponseRecipientModel>();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models
{
    public class EMailForgotPasswordData : EMailData
    {
        #region Properties

        public String RestoreLink { get; set; }

        #endregion

        #region Constructor

        public EMailForgotPasswordData()
        {

        }

        #endregion
    }
}

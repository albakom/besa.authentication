﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models
{
    public abstract class EMailData
    {
        #region Properties

        /// <summary>
        /// email adress for recipent
        /// </summary>
        [Required]
        public String ToMailAdress { get; set; }

        /// <summary>
        /// Nice name to display in email header. Can be null
        /// </summary>
        public String ToName { get; set; }

        #endregion

        #region Constructor

        public EMailData()
        {

        }

        #endregion
    }
}

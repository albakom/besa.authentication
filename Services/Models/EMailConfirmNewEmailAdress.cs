﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services.Models
{
    public class EMailConfirmNewEmailAdress : EMailData
    {
        #region Properties

        public String ConfirmationLink { get; set; }

        #endregion

        #region Constructor

        public EMailConfirmNewEmailAdress()
        {

        }

        #endregion
    }
}

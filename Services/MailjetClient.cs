﻿using Albakom.Besa.Authentication.Services.Models.Mailjet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Albakom.Besa.Authentication.Services
{
    public class MailjetClient
    {
        #region Properties

        private const string _mailJetBaseUrL = "https://api.mailjet.com/v3.1/";

        private readonly string _publicKey;
        private readonly string _apiKey;

        private static Dictionary<MailJetResoucers, String> _responseMapper;


        private enum MailJetResoucers
        {
            Send,
        }

        #endregion

        #region Constructor

        static MailjetClient()
        {
            _responseMapper = new Dictionary<MailJetResoucers, string>();
            _responseMapper.Add(MailJetResoucers.Send, "send");
        }

        public MailjetClient(String publicKey, String apiKey)
        {
            this._publicKey = publicKey;
            this._apiKey = apiKey;
        }

        #endregion

        #region Methods

        public  async Task<MailJetSendResponseModel> SendMail(MailjetSendMessageRequestModel requestModel)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_mailJetBaseUrL);

            Byte[] authAsASCII = Encoding.ASCII.GetBytes($"{_publicKey}:{_apiKey}");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authAsASCII));

            StringContent content = new StringContent(JsonConvert.SerializeObject(requestModel), new UTF8Encoding(), "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync(_responseMapper[MailJetResoucers.Send], content);

            String rawResponeContent = String.Empty;

            try
            {
                rawResponeContent = await responseMessage.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {

            }

            if (responseMessage.IsSuccessStatusCode == false)
            {
                MailJetSendResponseModel responseModel = new MailJetSendResponseModel { HasError = true, Error = rawResponeContent, StatusCode = (Int32)responseMessage.StatusCode };
                return responseModel;
            }

            MailJetSendResponseModel result = JsonConvert.DeserializeObject<MailJetSendResponseModel>(rawResponeContent);
            result.StatusCode = (Int32)responseMessage.StatusCode;
            result.HasError = false;

            return result;
        }

        #endregion

    }
}

﻿using Albakom.Besa.Authentication.Services.Models;
using Albakom.Besa.Authentication.Services.Models.Mailjet;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class MailjetBasedEmailSender : IEmailSender
    {
        #region Fields

        private String _apiKey;
        private readonly ILogger<MailjetBasedEmailSender> _logger;
        private readonly MailjetTemplateResolver _templateResolver;
        private String _publicKey;

        #endregion

        #region Properties

        #endregion


        public MailjetBasedEmailSender(ILogger<MailjetBasedEmailSender> logger, MailjetTemplateResolver templateResolver, String publicKey, String apiKey)
        {
            if (string.IsNullOrEmpty(apiKey))
            {
                throw new ArgumentException("message", nameof(apiKey));
            }

            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._templateResolver = templateResolver ?? throw new ArgumentNullException(nameof(templateResolver));
            _publicKey = publicKey ?? throw new ArgumentNullException(nameof(publicKey));
            _apiKey = apiKey;
        }

        private MailJetSendMessageItemModel GetRequestModelForRestore(EMailForgotPasswordData data)
        {
            MailJetSendMessageItemModel requestModel = new MailJetSendMessageItemModel(
            new MailJetRecipientModel("g.reinholz@albakom.de", "BESA Admin"),
            new MailJetRecipientModel(data.ToMailAdress, String.IsNullOrEmpty(data.ToName) == true ? data.ToMailAdress : data.ToName));
            requestModel.Variables.Add("rspw", data.RestoreLink);
            requestModel.Subject = "Passwort bei BESA wiederherstellen";

            return requestModel;
        }

        private MailJetSendMessageItemModel GetRequestModelForConfirm(EMailConfirmNewEmailAdress data)
        {
            MailJetSendMessageItemModel requestModel = new MailJetSendMessageItemModel(
           new MailJetRecipientModel("g.reinholz@albakom.de", "BESA Admin"),
           new MailJetRecipientModel(data.ToMailAdress, String.IsNullOrEmpty(data.ToName) == true ? data.ToMailAdress : data.ToName));
            requestModel.Variables.Add("ConfirmationLink", data.ConfirmationLink);
            requestModel.Subject = "Email-Adresse bei BESA bestätigen";

            return requestModel;
        }

        public async Task SendEmailAsync(EMailReasons reason, EMailData data)
        {
            _logger.LogTrace("get mail template id with reason {0}", reason);

            if (_templateResolver.CheckIfTemplateIsAvaible(reason) == false)
            {
                _logger.LogError("no template id for reason {0} found", reason);
                return;
            }

            Int32 templateId = _templateResolver.GetTemplateId(reason);
            MailJetSendMessageItemModel requestModel = null;
            switch (reason)
            {
                case EMailReasons.PasswordForgotten:
                    requestModel = GetRequestModelForRestore(data as EMailForgotPasswordData);
                    break;
                case EMailReasons.ConfirmNewEmailAdress:
                    requestModel = GetRequestModelForConfirm(data as EMailConfirmNewEmailAdress);
                    break;
                default:
                    break;
            }

            if (requestModel == null) { return; }

            requestModel.TemplateLanguage = true;
            requestModel.TemplateID = templateId;

            MailjetClient client = new MailjetClient(_publicKey, _apiKey);

            MailJetSendResponseModel response = await client.SendMail(new MailjetSendMessageRequestModel(requestModel));
            if (response.HasError == false)
            {
                _logger.LogInformation("mail send successfull. total messages: {0}", response.Messages.Count());
            }
            else
            {
                _logger.LogError("unable to send mail. code: {0}, info {1}", response.StatusCode, response.GetErrorInfo());
            }
        }
    }
}

﻿using Albakom.Besa.Authentication.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Albakom.Besa.Authentication.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(EMailReasons reason, EMailData data);
    }
}
